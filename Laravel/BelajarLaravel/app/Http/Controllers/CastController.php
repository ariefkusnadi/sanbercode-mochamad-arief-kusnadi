<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }
//tambah
    public function store(Request $request)
    {
      $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => "Nama harus diisi tidak boleh kosong!!",
            'umur.required' => "Umur harus diisi tidak boleh kosong!!",
            'bio.required' => "Bio harus diisi tidak boleh kosong!!",
//tampil DB
    ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.detail' , ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);
        
        return view('cast.edit' , ['cast' => $cast]);
    }

//update
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => "Nama harus diisi tidak boleh kosong!!",
            'umur.required' => "Umur harus diisi tidak boleh kosong!!",
            'bio.required' => "Bio harus diisi tidak boleh kosong!!",
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );

        return redirect('/cast');
    }
//destroy
    public function destroyed($id)
    {
        $deleted = DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }

}
