<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }

    public function wellcome(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.wellcome',['namaDepan' => $namaDepan , 'namaBelakang' => $namaBelakang]);
    }
}