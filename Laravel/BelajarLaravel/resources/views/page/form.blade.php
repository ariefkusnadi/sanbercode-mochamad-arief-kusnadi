@extends('layouts.master')
@section('title')
    Selamat Datang
@endsection

@section('sub-title')
    halooo
@endsection
@section('content')
        <h1>Buat Account Baru!</h1>

        <h2>Sign Up Form</h2>

        <form action="/welcome" method="post">
            @csrf
            <label>First Name : </label><br><br>
            <input type="text" name="fname"><br><br>

            <label>Last Name : </label><br><br>
            <input type="text" name="lname"><br><br>

            <label>Gender :</label><br><br>
            <input type="radio">Male<br>
            <input type="radio">Female<br>
            <input type="radio">Other<br><br>

            <label>Nationality :</label><br><br>
            <select name="Nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Indonesia">Malaysian</option>
                <option value="Indonesia">Singapore</option>
                <option value="Indonesia">Australian</option>
            </select> <br><br>

            <label>Language Spoken :</label><br><br>
            <input type="checkbox" name="skill">Bahasa Indonesia<br>
            <input type="checkbox" name="skill">English<br>
            <input type="checkbox" name="skill">Other<br><br>

            <label>Bio : </label><br><br>
            <textarea name="message" rows="10" cols="30"></textarea>
            <br><br>

            <input type="submit" value="Sign Up">
        </form>
 @endsection