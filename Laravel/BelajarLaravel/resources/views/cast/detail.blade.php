@extends('layouts.master')
@section('title')
    Halaman Detail
@endsection

@section('sub-title')
    Detail
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<h2>Umur {{$cast->umur}}</h2>
<h3>Biodata</h3>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection