<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'dashboard'] );

Route::get('/register', [AuthController::class,'register'] );
Route::post('/welcome', [AuthController::class,'wellcome'] );

Route::get('/data-table' , function(){
    return view('page.data-table');
});

Route::get('/table' , function(){
    return view('page.table');
});
// testing master template


//CRUD

//Create Data
//route mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan inputan kedalam database cast
Route::post('/cast', [CastController::class, 'store']);

//READ Data
//Route mengarah ke halaman tampil semua data yang ada di tabel cast
Route::get('/cast', [CastController::class, 'index']);
//Route detail cast berdasarkan_id
Route::get('/cast/{id}', [CastController::class, 'show']);

//UPDATE data
//Route mengarah ke form edit cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route untuk edit data berdasarkan ID cast
Route::put('/cast/{id}', [CastController::class, 'update']);

//DELETE data
Route::delete('/cast/{id}', [CastController::class, 'destroyed']);



