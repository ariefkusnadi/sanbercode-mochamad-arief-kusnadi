<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal = new Animal("Shaun");
echo "Name : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->coldblooded . "<br><br>";

$animal = new Frog("Buduk");
echo "Name : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->coldblooded . "<br>";
echo "Jump : " . $animal->jump() . "<br>";

$animal = new Ape("Kera Sakti");
echo "Name : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->coldblooded . "<br>";
echo "Yell : " . $animal->yell() . "<br>";

?>