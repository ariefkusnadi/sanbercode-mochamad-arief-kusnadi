<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php 
/*SOAL 1
*/  
        echo "<h3> SOAL NO 1</h3>";

        echo "<h4> Kalimat 1</h4>";
        $kalimat1 = "Hello PHP!";
        echo "Kalimat pertama : " . $kalimat1 . "<br>";
        echo "Panjang Kalimat 1 : " . strlen($kalimat1) . "<br>";
        echo "Jumlah kata kalimat 1 : " . str_word_count($kalimat1) . "<br>";

        echo "<h4> Kalimat 2</h4>";
        $kalimat2 = "I'm ready for the challenges";
        echo "Kalimat kedua : " . $kalimat2 . "<br>";
        echo "Panjang Kalimat 2 : " . strlen($kalimat2) . "<br>";
        echo "Jumlah kata kalimat 2 : " . str_word_count($kalimat2) . "<br>";

/*SOAL 2
*/
        echo "<h3> SOAL NO 2</h3>";

        echo "<h4> Kalimat 3</h4>";
        $string2 = "I love PHP";
        echo "Kalimat ketiga : " . $string2 . "<br>";
        echo "Kata 1 Kalimat ketiga : " . substr($string2,0,1) . "<br>";
        echo "Kata 2 Kalimat ketiga : " . substr($string2,2,4) . "<br>";
        echo "Kata 3 Kalimat ketiga : " . substr($string2,7,3) . "<br>";

/*SOAL 3
*/
        echo "<h3> SOAL NO 3</h3>";

        echo "<h4> Kalimat 4</h4>";
        $string3 = "PHP is old but sexy!";
        echo " Kalimat keempat : " . $string3 . "<br>";
        echo " Ganti string kalimat ke 4 : " . str_replace("sexy!","awesome", $string3);

    ?>
</body>
</html>