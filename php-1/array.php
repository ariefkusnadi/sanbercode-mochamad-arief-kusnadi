<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> SOAL 1 </h3>";

    echo "<h4> Kids </h4>";
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    print_r($kids);

    echo "<br>";

    echo "<h4> Adults </h4>";
    $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    print_r($adults);

    echo "<br>";
    echo "<h3> SOAL 2</h3>";
    
    echo "Data Kids <br>";
    echo "Total Kids : " . count($kids) . "<br>";
    echo "<ol>";
    echo "<li>" . $kids[0] ."</li>";
    echo "<li>" . $kids[1] ."</li>";
    echo "<li>" . $kids[2] ."</li>";
    echo "<li>" . $kids[3] ."</li>";
    echo "<li>" . $kids[4] ."</li>";
    echo "<li>" . $kids[5] ."</li>";
    echo "</ol>";

    echo "Data Adults <br>";
    echo "Total Adults : " . count($kids) . "<br>";
    echo "<ol>";
    echo "<li>" . $adults[0] ."</li>";
    echo "<li>" . $adults[1] ."</li>";
    echo "<li>" . $adults[2] ."</li>";
    echo "<li>" . $adults[3] ."</li>";
    echo "<li>" . $adults[4] ."</li>";
    echo "</ol>";
    
echo "<h3> SOAL 3</h3>";
$bioAdults = [
    ["Nama" => "Will Byers", "Age" => "12", "Aliases" => "Will the Wise", "Status" => "Alive"],
    ["Nama" => "Mike Wheeler", "Age" => "12", "Aliases" => "Dungeon Master", "Status" => "Alive"],
    ["Nama" => "Jim Hopper", "Age" => "43", "Aliases" => "Chief Hopper", "Status" => "Deceased"],
    ["Nama" => "Eleven", "Age" => "12", "Aliases" => "El", "Status" => "Alive"],
];

echo "<pre>";
print_r($bioAdults);
echo"</pre>";



    ?>
</body>

</html>